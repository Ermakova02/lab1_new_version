import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class Connector {
    private static final String MESSAGE_TEXT = "Message from Task Count";
    private static final int SEND_FREQ = 500;
    private static final int RECEIVE_DELAY = 1500;
    private static final int PRESENCE_DELAY = 3000;
    private static final int PACKET_MAX_SIZE = 1024;

    private String groupName;
    private int port;
    private boolean forceRedraw;
    private boolean stopped;
    private List<IPAddressTime> list;

    Connector(String groupName, int port) {
        this.groupName = groupName;
        this.port = port;
        forceRedraw = true;
        stopped = false;
        list = new ArrayList<>();
    }

    private void redraw() {
        System.out.println("Run programs:");
        for (int i = 0; i < list.size(); i++)
            System.out.println(list.get(i).getAddress());
    }

    private void updateIPAddress(String address) {
        IPAddressTime ip = new IPAddressTime(address);
        if (list.contains(ip)) {
            list.get(list.indexOf(ip)).setUpdatedTime();
        } else {
            list.add(ip);
            forceRedraw = true;
        }
    }

    private void updateIPAddressesList() {
        long time = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            if (time - list.get(i).getTime() > PRESENCE_DELAY) {
                list.remove(list.get(i));
                forceRedraw = true;
            }
        }
        if (forceRedraw) redraw();
    }

    public void start() {
        MulticastSocket receiveSocket = null;
        DatagramSocket sendSocket = null;
        InetAddress groupAddress = null;
        try {
            receiveSocket = new MulticastSocket(port);
            sendSocket = new DatagramSocket();
            groupAddress = InetAddress.getByName(groupName);
            receiveSocket.joinGroup(groupAddress);
            receiveSocket.setSoTimeout(RECEIVE_DELAY);
            byte[] sendMsg = MESSAGE_TEXT.getBytes();
            forceRedraw = true;
            while (!stopped) {
                // Send block
                DatagramPacket sendPacket = new DatagramPacket(sendMsg, sendMsg.length);
                sendPacket.setAddress(groupAddress);
                sendPacket.setPort(port);
                sendSocket.send(sendPacket);

                // Receive block
                DatagramPacket receivePacket = new DatagramPacket(new byte[PACKET_MAX_SIZE], PACKET_MAX_SIZE);
                receiveSocket.receive(receivePacket);
                String receiveMsg = new String(receivePacket.getData(), receivePacket.getOffset(), receivePacket.getLength());
                if (receiveMsg.equals(MESSAGE_TEXT))
                    updateIPAddress(receivePacket.getAddress().toString());
                updateIPAddressesList();
                forceRedraw = false;
                Thread.sleep(SEND_FREQ);
            }
            receiveSocket.leaveGroup(groupAddress);
            receiveSocket.close();
            sendSocket.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
