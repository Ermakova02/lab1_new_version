import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.List;

public class TaskCount {
    private static final String DEFAULT_GROUP = "230.0.0.1";
    private static final int DEFAULT_PORT = 12345;

    public static void main(String[] args) {
        String groupName = DEFAULT_GROUP;
        int port = DEFAULT_PORT;
        switch (args.length) {
            case 1: {
                groupName = args[0];
            } break;
            case 2: {
                groupName = args[0];
                port = Integer.valueOf(args[1]);
            } break;
        }
        Connector connector = new Connector(groupName, port);
        connector.start();
    }
}
